//
//  ViewController.m
//  WalkAndChange
//
//  Created by Vahe Vanoyan on 4/7/15.
//  Copyright (c) 2015 Vahe Vanoyan. All rights reserved.
//

#import "ViewController.h"
#import <CoreMotion/CoreMotion.h>
#import "Model.h"
#import <Parse/Parse.h>

@interface ViewController () <UIAlertViewDelegate>
@property (nonatomic, strong) UILabel *AllStepsName;
@property (nonatomic, strong) UILabel *AllSteps;
@property (nonatomic, strong) UIButton *CollectButton;
@property (nonatomic, strong) UILabel *AccountInfoName;
@property (nonatomic, strong) UILabel *StepDollarsON;
@property (nonatomic, strong) UILabel *StepDollarsToCollect;
@property (nonatomic, strong) CMPedometer *ped;
@property(nonatomic)  NSInteger StepDollars, StepsToCollect, StepsToday,      temp, temp1;
@property(nonatomic, strong) NSString *CollectText, *OnText, *StepsText;
@property (nonatomic, strong) NSTimer *myTimer;
@property (nonatomic, strong)NSNumber *steps;
@property (nonatomic, strong) NSDate *today, *StartDay, *SecondsBefore, *collectDate, *tempDate;
@property (nonatomic, strong)UITextField *myTextField;
@property (nonatomic, strong) UIAlertView *alertError, *alertError2;
@property (nonatomic, strong) PFQuery *query1;
@property (nonatomic, assign) BOOL alertBool, textBool;




@end

@implementation ViewController

-(instancetype)init
{
    self = [super init];
    if(self)
    {
        
        
        
        
        
        self.StepDollars=[Model sharedInstance].StepDollarsDrive;
        if([Model sharedInstance].LastCollectDrive==nil)
            self.collectDate=self.today;
        else
        {
          self.collectDate=[Model sharedInstance].LastCollectDrive;
        }
        
        self.today = [NSDate date];
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        [gregorian setLocale:[NSLocale currentLocale]];
        NSDateComponents *nowComponents = [gregorian components:( NSCalendarUnitMonth | NSCalendarUnitDay| NSCalendarUnitYear | NSCalendarUnitHour |       NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:self.today];
        [nowComponents setHour:0];
        [nowComponents setMinute:0];
        [nowComponents setSecond:0];
        
        self.StartDay = [gregorian dateFromComponents:nowComponents];
        
      
        
       // self.collectDate=[Model sharedInstance].LastCollectDrive;
        
      
        
        self.ped = [[CMPedometer alloc] init];
      
   
        self.view.backgroundColor=[UIColor colorWithRed:43.0f/255.0f
                                                  green:128.0f/255.0f
                                                   blue:62.0f/255.0f
                                                  alpha:1.0f];
        self.title=@"WalkAndChange";
        
        self.CollectButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [self.CollectButton addTarget:self
                               action:@selector(collect)
         
                     forControlEvents:UIControlEventTouchUpInside];
      
        [self.CollectButton setTitle:@"COLLECT" forState:UIControlStateNormal];
        self.CollectButton.frame = CGRectMake(175, 230, 160, 80);
    //   self.CollectButton.enabled=NO;

        [self.view addSubview:self.CollectButton];
        

        
        self.StepDollarsToCollect=[[UILabel alloc] initWithFrame:CGRectMake(120, 250, 250, 30)];
       
        [self.view addSubview:self.StepDollarsToCollect];
        
        
        
        self.AccountInfoName=[[UILabel alloc] initWithFrame:CGRectMake(55, 50, 250, 30)];
        self.AccountInfoName.text=@"Total StepDollars On Account: " ;
        self.AccountInfoName.textColor = [UIColor blackColor];
        self.AccountInfoName.font=[self.AccountInfoName.font fontWithSize:15];
        [self.view addSubview:self.AccountInfoName];
        
        
        
        self.StepDollarsON=[[UILabel alloc] initWithFrame:CGRectMake(270, 50, 75, 30)];
        self.OnText=[NSString stringWithFormat:@"%ld", [Model sharedInstance].StepDollarsDrive];
        self.StepDollarsON.text=self.OnText;
        self.StepDollarsON.textColor = [UIColor blackColor];
        self.StepDollarsON.font=[self.StepDollarsON.font fontWithSize:15];
        [self.view addSubview:self.StepDollarsON];
        
        self.AllStepsName = [[UILabel alloc] initWithFrame:CGRectMake(100, 450, 200, 30)];
        self.AllStepsName.text = @"Total Steps Done Today";
        self.AllStepsName.textColor = [UIColor blackColor];
        [self.view addSubview:self.AllStepsName];
        
        
        
        
       
        self.AllSteps = [[UILabel alloc] initWithFrame:CGRectMake(175, 500, 200, 30)];
              [self.view addSubview:self.AllSteps];
        
        
        self.myTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                        target:self
                                                      selector:@selector(updateSteps)
                                                      userInfo:nil
                                                       repeats:YES];
        
        
        
        
    }
    return self;
    
    
}
-(void)collect
    {
       
        
        
        
       
        self.collectDate=[NSDate date ];
        self.StepDollars+=self.StepsToCollect;
        
        self.OnText=[NSString stringWithFormat:@"%ld", self.StepDollars];
        self.StepsToCollect=0;
             self.temp=0;
        
        self.CollectText =[NSString stringWithFormat:@"%ld", self.StepsToCollect];
       
        
         
   
                NSLog(@"STEPS: %@",   [Model sharedInstance].LastCollectDrive);
             [Model sharedInstance].LastCollectDrive=self.collectDate;
             [Model sharedInstance].StepDollarsDrive=self.StepDollars;
             [[Model sharedInstance] saveToDisk];
            
        NSLog(@"STEPS: %ld", (long)[Model sharedInstance].StepDollarsDrive);
        
        if(true){
        
        
        }
        
        
}

-(void)setCollectText:(NSString *)CollectText
{
    _CollectText = CollectText;

    self.StepDollarsToCollect.text=_CollectText;
}
-(void)setOnText:(NSString *)OnText{
    _OnText=OnText;
    self.StepDollarsON.text=_OnText;
}
-(void)setStepsText:(NSString *)StepsText{
    _StepsText=StepsText;
    self.AllSteps.text=_StepsText;}


- (void)updateSteps {
    self.OnText=[NSString stringWithFormat:@"%ld", [Model sharedInstance].StepDollarsDrive];
    
    
    if([CMPedometer isStepCountingAvailable])
    {
        
        if(self.collectDate==nil)
        {self.tempDate=self.today;}
        else
        { self.tempDate=self.collectDate;}
        
        [self.ped queryPedometerDataFromDate:self.tempDate toDate:[NSDate date] withHandler:^(CMPedometerData *pedometerData, NSError *error) {
            
            self.steps = pedometerData.numberOfSteps;
            self.temp = [self.steps integerValue];
            
            if(self.temp>=1000){
                self.temp=1000;
            }
            self.StepsToCollect= self.temp;
            self.temp=0;
        }];
    }
    
    if( [CMPedometer isStepCountingAvailable])
    {
        [self.ped queryPedometerDataFromDate:self.StartDay toDate:[NSDate date] withHandler:^(CMPedometerData *pedometerData, NSError *error) {
            
            self.steps = pedometerData.numberOfSteps;
            self.temp1 = [self.steps integerValue];
            self.StepsToday=self.temp1;
            
        }];
    }
    self.StepsText=[NSString stringWithFormat:@"%ld", self.StepsToday];
    self.AllSteps.text = self.StepsText;
    self.AllSteps.font=[self.AllSteps.font fontWithSize:20];
    self.AllSteps.textColor = [UIColor blackColor];
    
    self.CollectText=[NSString stringWithFormat:@"%ld", self.StepsToCollect];
    self.StepDollarsToCollect.text=self.CollectText;
    self.StepDollarsToCollect.textColor = [UIColor blackColor];
    self.StepDollarsToCollect.font=[self.StepDollarsToCollect.font fontWithSize:25];
}

- (void)askUsername {
    if([Model sharedInstance].run==NO && self.alertBool==NO && self.textBool==NO){
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Enter You Username" message:@"Please Enter Your Username" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
        av.alertViewStyle = UIAlertViewStylePlainTextInput;
        [av show];
    }
    else if([Model sharedInstance].run==NO && self.alertBool==YES){
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"The Username Is Being Already Used" message:@"Please Enter Your Username" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
        av.alertViewStyle = UIAlertViewStylePlainTextInput;
        [av show];
        
        
    }
    if([Model sharedInstance].run==NO && self.alertBool==NO && self.textBool==YES){
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"User Input Error" message:@"Please Enter Your Username" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
        av.alertViewStyle = UIAlertViewStylePlainTextInput;
        [av show];
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
    [self askUsername];
    [self updateSteps];
    }

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0 && [alertView textFieldAtIndex:0].text.length!=0){
        
        [Model sharedInstance].UserName=[alertView textFieldAtIndex:0].text;
       
        
        self.query1=[PFQuery queryWithClassName:@"Users"];
        [self.query1 whereKey:@"Name" equalTo:[Model sharedInstance].UserName];
        [self.query1 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
          
            if(objects.count>0){
                [Model sharedInstance].run = NO;
                [[Model sharedInstance] saveToDisk ];
               // [self.alertError show];
                self.alertBool=YES;
                [self askUsername];
                               }
          
        
         }];
        [Model sharedInstance].run = YES;
        [[Model sharedInstance] saveToDisk ];
    }
    else
    
    {
        self.alertBool=NO;
        self.textBool=YES;
        [self askUsername];
    }

}
@end
    
