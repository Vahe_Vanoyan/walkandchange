//
//  Donate.m
//  EmptyiPhoneProject
//
//  Created by Vahe Vanoyan on 3/26/15.
//  Copyright (c) 2015 CS3200. All rights reserved.
//

#import "Donate.h"
#import <Parse/Parse.h>
@interface Donate ()
@property (nonatomic, strong) UILabel *Textlabel1, *Textlabel2, *Textlabel3, *Textlabel4, *InfoLabel;
@property (nonatomic, strong) UILabel *ConvertTextLabel;
@property (nonatomic) NSInteger StepDollars, DonatedByUser, totalDonated;
@property (nonatomic,strong) UITextField *TextField1,*TextField2,*TextField3,*TextField4;
@property (nonatomic, strong) UIButton *Donate;
@property (nonatomic, strong) UIAlertView *alertError, *alertT;
@property (nonatomic, strong) NSString *TextInfo, *NameUser;
@property (nonatomic, strong) NSMutableArray *Names, *Amount;
@property (nonatomic, strong) PFQuery *query, *UserQ, *query1;

@end

@implementation Donate

-(instancetype)init
{
    self = [super init];
    if(self)
    {
        [super viewDidLoad];
        
        
        self.Names=[NSMutableArray new];
        self.Amount=[NSMutableArray new];

        self.title=@"Donate";
        self.view.backgroundColor=[UIColor colorWithRed:43.0f/255.0f
                                                  green:128.0f/255.0f
                                                   blue:62.0f/255.0f
                                                  alpha:1.0f];
        self.StepDollars=[Model sharedInstance].StepDollarsDrive;
        
        self.alertError = [[UIAlertView alloc] initWithTitle:@"User Input Error"
                                                message:@"StepMoney Donated Can Not Be Larger Than Your Total StepMoney"
                                               delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
        
        self.alertT = [[UIAlertView alloc] initWithTitle:@"Thank You"
                                                     message:@"Thank You For Your Input"
                                                    delegate:self
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        
        self.InfoLabel=[[UILabel alloc] initWithFrame:CGRectMake(55, 50, 275, 30)];
        self.TextInfo=[ NSString stringWithFormat: @"Total StepDollars On Account:  %ld " , [Model sharedInstance].StepDollarsDrive];
        self.InfoLabel.text=self.TextInfo;
        self.InfoLabel.textColor = [UIColor blackColor];
        self.InfoLabel.font=[self.InfoLabel.font fontWithSize:15];
        [self.view addSubview:self.InfoLabel];
        

        self.Textlabel1=[[UILabel alloc] initWithFrame:CGRectMake(35, 145, 275, 30)];
              self.Textlabel1.textColor = [UIColor blackColor];
        self.Textlabel1.font=[self.Textlabel1.font fontWithSize:15];
        [self.view addSubview:self.Textlabel1];
        
     
        self.TextField1 = [[UITextField alloc] initWithFrame:CGRectMake(200, 147, 75, 25)];
        self.TextField1.borderStyle = UITextBorderStyleRoundedRect;	// makes the text field more visible
        self.TextField1.keyboardType = UIKeyboardTypeDecimalPad;	// change the keyboard to what you want, UIKeyboardType...
        self.TextField1.placeholder = @"Amount";	// text that is displayed inside the field when it's empty
        [self.view addSubview:self.TextField1];
        
        
        
        
        
        self.Textlabel2=[[UILabel alloc] initWithFrame:CGRectMake(35, 180, 275, 30)];
               self.Textlabel2.textColor = [UIColor blackColor];
        self.Textlabel2.font=[self.Textlabel2.font fontWithSize:15];
        [self.view addSubview:self.Textlabel2];
        
        
        self.TextField2 = [[UITextField alloc] initWithFrame:CGRectMake(200, 182, 75, 25)];
        self.TextField2.borderStyle = UITextBorderStyleRoundedRect;	// makes the text field more visible
        self.TextField2.keyboardType = UIKeyboardTypeDecimalPad;	// change the keyboard to what you want, UIKeyboardType...
        self.TextField2.placeholder = @"Amount";	// text that is displayed inside the field when it's empty
        [self.view addSubview:self.TextField2];

        
        
        
        self.Textlabel3=[[UILabel alloc] initWithFrame:CGRectMake(35, 215, 275, 30)];
               self.Textlabel3.textColor = [UIColor blackColor];
        self.Textlabel3.font=[self.Textlabel3.font fontWithSize:15];
        [self.view addSubview:self.Textlabel3];
        
        
        self.TextField3 = [[UITextField alloc] initWithFrame:CGRectMake(200, 217, 75, 25)];
        self.TextField3.borderStyle = UITextBorderStyleRoundedRect;	// makes the text field more visible
        self.TextField3.keyboardType = UIKeyboardTypeDecimalPad;	// change the keyboard to what you want, UIKeyboardType...
        self.TextField3.placeholder = @"Amount";	// text that is displayed inside the field when it's empty
        [self.view addSubview:self.TextField3];
        
        
        
        self.Donate = [[UIButton alloc] initWithFrame:CGRectMake(135, 300, 100, 44)];
        [self.Donate setTitle:@"Donate" forState:UIControlStateNormal];
        [self.Donate setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [self.Donate setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        [self.Donate addTarget:self action:@selector(donate) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.Donate];
        
        
        [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard) ]];
     
        self.query = [PFQuery queryWithClassName:@"Place"];
        [self.query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            for(PFObject* object in objects){
                [self.Names addObject:object[@"Name"]];
                [self.Amount addObject:object[@"Amount"]];
            }
            self.Textlabel1.text=self.Names[0];
            self.Textlabel2.text=self.Names[1];
            self.Textlabel3.text=self.Names[2];

        }];
    }
    return self;
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
     self.TextInfo=[ NSString stringWithFormat: @"Total StepDollars On Account:  %ld " , [Model sharedInstance].StepDollarsDrive];

}
-(void)dismissKeyboard{

    [self.view endEditing:YES];


}
-(void)donate
{
    NSInteger First, Second, Third;
    First=[self.TextField1.text integerValue];
    Second=[self.TextField2.text integerValue];
    Third=[self.TextField3.text integerValue];
    NSInteger total;
    total=First+Second+Third;
   
    
    if(total>[Model sharedInstance].StepDollarsDrive){
    
    [self.alertError show];
        
        First=0;
        Second=0;
        Third=0;
        
        self.TextField1.text=@"";
        self.TextField2.text=@"";
        self.TextField3.text=@"";
    }
  else
    {       if(total==0){
            [self dismissKeyboard];
            return;}
        [Model sharedInstance].StepDollarsDrive-=total;
         [[Model sharedInstance] saveToDisk];
        self.TextInfo=[ NSString stringWithFormat: @"Total StepDollars On Account:  %ld " , [Model sharedInstance].StepDollarsDrive];
        self.TextField1.text=@"";
        self.TextField2.text=@"";
        self.TextField3.text=@"";
               [self.query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                   NSMutableArray *temp=[ NSMutableArray new];
                      for(PFObject* object in objects){
                [self.Names addObject:object[@"Name"]];
                [self.Amount addObject:object[@"Amount"]];
            }
            temp=self.Amount;
            temp[0]=@(First +  [temp[0] integerValue]);
            temp[1]=@(Second + [temp[1] integerValue]);
            temp[2]=@(Third + [temp[2] integerValue]);
                   NSInteger i=0;
                   for( PFObject* object in objects ){
                   object[@"Amount"]=temp[i];
                       i++;
                       [object saveInBackground];}
               }];
        
        
        self.query1=[PFQuery queryWithClassName:@"Users"];
        [self.query1 whereKey:@"Name" equalTo:[Model sharedInstance].UserName];
         [self.query1 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
             NSNumber *tempo;
             if(objects.count>0){
             
               tempo = objects[0][@"Amount"];
                 tempo=@(total + [tempo integerValue]);
                 objects[0][@"Amount"]=tempo;
                 [objects[0] saveInBackground];
             }
                else
                {
                
                    PFObject *user=[PFObject objectWithClassName:@"Users"];
                    user[@"Name"]=[Model sharedInstance].UserName;
                    user[@"Amount"]=@(total);
                    [user saveInBackground];
                }
         }];
        
        
        
        [self dismissKeyboard];
        [self.alertT show ];
    }
}
-(void)setTextInfo:(NSString *)TextInfo{
    _TextInfo=TextInfo;
    self.InfoLabel.text=_TextInfo;
}


@end
