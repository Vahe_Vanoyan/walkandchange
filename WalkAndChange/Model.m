

#import "Model.h"

@implementation Model


#pragma mark - Saving to Disk
+(Model*)sharedInstance
{
    static Model *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        
        // initalize the class here:
        //instance.LastCollectDrive=[NSDate date ];
        //instance.StepDollarsDrive=0;
        [instance readFromDisk];
    });
    
    return instance;
}
// get the directory for saving files
+(NSString*)applicationDocumentsDirectory
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
	return [basePath stringByAppendingPathComponent:@"data"];
}

// call this to get your data back from the disk
-(void)readFromDisk
{
	NSString *dataPath = [Model applicationDocumentsDirectory];
	NSData *codedData = [[NSData alloc] initWithContentsOfFile:dataPath];	// check to see if we have data at our file path
	if (codedData)
	{
		NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:codedData];
		Model *modelData = [unarchiver decodeObjectForKey:@"Model"];	// this will call your custom initWithCoder method
		
		// -------------------------- TODO 3 --------------------------
		// you'll get back a newly created "Model" object that you'll have to copy over
		//		This means that you don't have to save everything in your class
		//			Example: self.yourPropertyName = modelData.yourPropertyName;
		self.StepDollarsDrive = modelData.StepDollarsDrive;
		self.LastCollectDrive = modelData.LastCollectDrive;
        self.run=modelData.run;
        self.UserName=modelData.UserName;
       
	}
}

// call this to save any changes made
-(void)saveToDisk
{
	NSString *dataPath = [Model applicationDocumentsDirectory];
	NSMutableData *data = [[NSMutableData alloc] init];
	NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
	[archiver encodeObject:self forKey:@"Model"];	// this will call your custom encodeWithCoder method
	[archiver finishEncoding];
	[data writeToFile:dataPath atomically:YES];
}

// this is encode your data into NSData so it can save it
-(void)encodeWithCoder:(NSCoder*)encoder
{
	// -------------------------- TODO 1 --------------------------
	// encode the objects you want saved -- rember the key!
	//		Example: [encoder encodeObject:self.yourPropertyName forKey:@"someKey"];	// if your property is a string/object
	//			or:  [encoder encodeInteger:self.yourPropertyName forKey:@"someDIFFERENTKey"];	// if your property is an int
   // self.LastCollectDrive=[NSDate date];
	[encoder encodeInteger:self.StepDollarsDrive forKey:@"obj1"];
	[encoder encodeObject:self.LastCollectDrive forKey:@"obj2"];
    [encoder encodeBool:self.run forKey:@"obj3"];
    [encoder encodeObject:self.UserName forKey:@"obj4"];

}

// this will decode your data from NSData into your original objects
-(id)initWithCoder:(NSCoder*)decoder
{
	self = [super init];
	
	// -------------------------- TODO 2 --------------------------
	// decode the objects that you saved in "encodeWithCoder" -- using the same key!
	//		Example: self.yourPropertyName = [decoder decodeObjectForKey:@"someKey"];	// if your property is a string/object
	//			or:  self.yourPropertyName = [decoder decodeIntegerForKey:@"someDIFFERENTKey"];	// if your property is an int
	self.StepDollarsDrive = [decoder decodeIntegerForKey:@"obj1"];
	self.LastCollectDrive = [decoder decodeObjectForKey:@"obj2"];
    self.run=[decoder decodeBoolForKey:@"obj3"];
    self.UserName = [decoder decodeObjectForKey:@"obj4"];
	
	return self;
}

@end
