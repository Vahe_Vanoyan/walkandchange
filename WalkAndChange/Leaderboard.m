//
//  Leaderboard.m
//  EmptyiPhoneProject
//
//  Created by Vahe Vanoyan on 3/26/15.
//  Copyright (c) 2015 CS3200. All rights reserved.
//

#import "Leaderboard.h"
#import <Parse/Parse.h>

@interface Leaderboard ()
@property (nonatomic, strong) UILabel *Textlabel1, *Textlabel2, *Textlabel3, *Textlabel4, *Textlabel5, *Textlabel6, *TextLabelPlaces, *TextLabelPeople ;
@property (nonatomic,strong) NSString  *PlacesText,*StringLabel1, *StringLabel2, *StringLabel3;
@property (nonatomic, strong) NSMutableArray *Names, *Amount, *Names1, *Amount1;
@property (nonatomic, strong) PFQuery *query1 , *query2;
@end
@implementation Leaderboard


-(instancetype)init
{
    self = [super init];
    if(self)
    {

        [super viewDidLoad];
        self.title=@"Leader Board";
        self.view.backgroundColor=[UIColor colorWithRed:43.0f/255.0f
                                                  green:128.0f/255.0f
                                                   blue:62.0f/255.0f
                                                  alpha:1.0f];
        
        self.TextLabelPlaces=[[UILabel alloc] initWithFrame:CGRectMake(125, 100, 275, 30)];
        self.TextLabelPlaces.textColor = [UIColor blackColor];
        self.TextLabelPlaces.font=[self.TextLabelPlaces.font fontWithSize:15];
        [self.view addSubview:self.TextLabelPlaces];
        
        self.Textlabel1=[[UILabel alloc] initWithFrame:CGRectMake(35, 145, 275, 30)];
        self.Textlabel1.textColor = [UIColor blackColor];
        self.Textlabel1.font=[self.Textlabel1.font fontWithSize:15];
        [self.view addSubview:self.Textlabel1];
        
        
        
        self.Textlabel2=[[UILabel alloc] initWithFrame:CGRectMake(35, 180, 275, 30)];
        self.Textlabel2.textColor = [UIColor blackColor];
        self.Textlabel2.font=[self.Textlabel2.font fontWithSize:15];
        [self.view addSubview:self.Textlabel2];
        
        
        
        self.Textlabel3=[[UILabel alloc] initWithFrame:CGRectMake(35, 215, 275, 30)];
        self.Textlabel3.textColor = [UIColor blackColor];
        self.Textlabel3.font=[self.Textlabel3.font fontWithSize:15];
        [self.view addSubview:self.Textlabel3];
        
        
        
        self.TextLabelPeople=[[UILabel alloc] initWithFrame:CGRectMake(125, 275, 275, 30)];
        self.TextLabelPeople.textColor = [UIColor blackColor];
        self.TextLabelPeople.font=[self.TextLabelPeople.font fontWithSize:15];
        [self.view addSubview:self.TextLabelPeople];
       
        
        
        
        self.Textlabel4=[[UILabel alloc] initWithFrame:CGRectMake(35, 320, 275, 30)];
        self.Textlabel4.textColor = [UIColor blackColor];
        self.Textlabel4.font=[self.Textlabel4.font fontWithSize:15];
        [self.view addSubview:self.Textlabel4];
        
        
        
        self.Textlabel5=[[UILabel alloc] initWithFrame:CGRectMake(35, 355, 275, 30)];
        self.Textlabel5.textColor = [UIColor blackColor];
        self.Textlabel5.font=[self.Textlabel5.font fontWithSize:15];
        [self.view addSubview:self.Textlabel5];
        
        
        self.Textlabel6=[[UILabel alloc] initWithFrame:CGRectMake(35, 390, 275, 30)];
        self.Textlabel6.textColor = [UIColor blackColor];
        self.Textlabel6.font=[self.Textlabel6.font fontWithSize:15];
        [self.view addSubview:self.Textlabel6];

  
    }
    return self;
}
-(void)setStringLabel1:(NSString *)StringLabel1{
    _StringLabel1=StringLabel1;
    self.Textlabel1.text=_StringLabel1;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.Names=[ NSMutableArray new];
    self.Amount=[NSMutableArray new];
    self.Names1=[ NSMutableArray new];
    self.Amount1=[NSMutableArray new];
    self.query1 = [PFQuery queryWithClassName:@"Place"];
     self.query2 = [PFQuery queryWithClassName:@"Users"];
    [self.query1 orderByDescending:@"Amount"];
    [self.query2 orderByDescending:@"Amount"];


    
    
    [self.query1 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
               for(PFObject* object in objects){
            [self.Names addObject:object[@"Name"]];
            [self.Amount addObject:object[@"Amount"]];
        }
                //problem organazing
        self.Textlabel1.text=[NSString stringWithFormat:@"%@  Amount: %li", self.Names [0], (long)[self.Amount[0] integerValue]];
        self.Textlabel2.text=[NSString stringWithFormat:@"%@  Amount: %li", self.Names [1], (long)[self.Amount[1] integerValue]];
        self.Textlabel3.text=[NSString stringWithFormat:@"%@  Amount: %li", self.Names [2], (long)[self.Amount[2] integerValue]];
    
    }];
    
    
    
    [self.query2 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        for(PFObject* object in objects){
            [self.Names1 addObject:object[@"Name"]];
            [self.Amount1 addObject:object[@"Amount"]];
        }
        //problem organazing
        self.Textlabel4.text=[NSString stringWithFormat:@"%@  Amount: %li", self.Names1 [0], (long)[self.Amount1[0] integerValue]];
        self.Textlabel5.text=[NSString stringWithFormat:@"%@  Amount: %li", self.Names1 [1], (long)[self.Amount1[1] integerValue]];
        self.Textlabel6.text=[NSString stringWithFormat:@"%@  Amount: %li", self.Names1 [2], (long)[self.Amount1[2] integerValue]];
        
    }];

    
    
    
    

   
    self.TextLabelPlaces.text=@"Top Places Donated";
    self.TextLabelPeople.text=@"Top Users  Donated";
    
    
}



@end
