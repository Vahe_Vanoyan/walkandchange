//
//  AppDelegate.h
//  WalkAndChange
//
//  Created by Vahe Vanoyan on 4/7/15.
//  Copyright (c) 2015 Vahe Vanoyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

